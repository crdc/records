![Build Status](https://gitlab.com/crdc/records/badges/master/build.svg)

---

# Records Microservice

A service that provides a GraphQL API for record documents stored in
MongoDB.

## Running

The easiest way to run the service is using the Docker container that's
available in the GitLab registry.

```sh
docker pull registry.gitlab.com/crdc/records:v1
docker run -it --rm -p 9000:9000 \
  -e CRDC_MONGODB_DATABASE="crdc" \
  -e CRDC_MONGODB_HOST="127.0.0.1" \
  -e CRDC_MONGODB_PORT="27017" \
  registry.gitlab.com/crdc/records:v1
```

## Developing

During development the service can be run using the included `gradle` script.

```sh
./gradlew bootRun
```

### Docker

To run locally in a Docker container use the following.

```sh
./gradlew bootJar
docker build -t crdc-records .
docker run -it --rm -p 9000:9000 \
  -e CRDC_MONGODB_DATABASE="crdc" \
  -e CRDC_MONGODB_HOST="127.0.0.1" \
  -e CRDC_MONGODB_PORT="27017" \
  crdc-records
```

## Publishing

This service is published to the repository container registry using these
steps.

```sh
./gradlew bootJar
docker build -t registry.gitlab.com/crdc/records:v1 .
docker push registry.gitlab.com/crdc/records:v1
```
