FROM openjdk:8

COPY build/libs/records-0.0.1-SNAPSHOT.jar /usr/bin/crdc-records

CMD ["java", "-jar", "/usr/bin/crdc-records"]
