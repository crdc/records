package ca.coanda.records

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RecordsApplication

fun main(args: Array<String>) {
	runApplication<RecordsApplication>(*args)
}
