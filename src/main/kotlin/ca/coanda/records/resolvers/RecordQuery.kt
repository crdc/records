package ca.coanda.records.resolvers

import ca.coanda.records.entity.Record
import ca.coanda.records.repository.RecordRepository
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.stereotype.Component

@Component
class RecordQuery(val recordRepository: RecordRepository,
                  private val mongoOperations: MongoOperations) : GraphQLQueryResolver {
    fun records(): List<Record> {
        val list = recordRepository.findAll()
        //for (item in list) {
        //    item.objects = getObjects(recordId = item.id)
        //}
        return list
    }

    //private fun getObjects(recordId: String): List<Object> {
    //    val query = Query()
    //    query.addCriteria(Criteria.where("recordId").`is`(recordId))
    //    return mongoOperations.find(query, Object::class.java)
    //}
}
