package ca.coanda.records.resolvers

import ca.coanda.records.entity.Record
import ca.coanda.records.repository.RecordRepository
import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import java.util.*

@Component
class RecordMutation (private val recordRepository: RecordRepository): GraphQLMutationResolver {
    fun newRecord(title: String, category: String): Record {
        val record = Record(title, category)
        record.id = UUID.randomUUID().toString()
        recordRepository.save(record)
        return record
    }

    fun deleteRecord(id: String): Boolean {
        recordRepository.deleteById(id)
        return true
    }

    fun updateRecord(id: String, title: String, category: String): Record {
        val record = recordRepository.findById(id)
        record.ifPresent {
            it.title = title
            if (category != "") {
                it.category = category
            }
            recordRepository.save(it)
        }
        return record.get()
    }
}