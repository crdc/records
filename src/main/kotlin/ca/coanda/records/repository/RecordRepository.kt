package ca.coanda.records.repository

import ca.coanda.records.entity.Record
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface RecordRepository : MongoRepository<Record, String>