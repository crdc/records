package ca.coanda.records.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "records")
data class Record (
        var title: String,
        var category: String
) {
    @Id
    var id: String = ""
}
